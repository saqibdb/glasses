//
//  ViewController.m
//  Glasses
//
//  Created by Hamza Temp on 12/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "ViewController.h"
#import "MyGlassesTableViewCell.h"
#import <LGBluetooth.h>
#import "GlassesPeripheral.h"
#import "MapViewController.h"
#import "AddNewDeviceTableViewCell.h"


@interface ViewController (){
    NSMutableArray *allFoundPerphs;
    GlassesPeripheral *selectedGlasses;
    UIImage *choosenImage;
}

@end

@implementation ViewController 
@synthesize tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];

    [LGCentralManager sharedInstance];

    self.glassesTableView.delegate = self;
    self.glassesTableView.dataSource = self;

   
    [self createNavigationTitleViewWithTitle:@""];

    //[self createNewGlasses];

    
    self.glassesTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Do any additional setup after loading the view, typically from a nib.
}


-(void)viewWillAppear:(BOOL)animated {
   _myGlassesArray = [GlassesPeripheral getAllGlasses];
    if (_myGlassesArray && _myGlassesArray.count) {
        self.glassesTableView.hidden = NO;
        [self.glassesTableView reloadData];
    }
    [self scanWithTime:2];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Table view data source



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (_myGlassesArray.count) {
        return _myGlassesArray.count + 1;
    }
    else{
        return 0;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row < _myGlassesArray.count) {
        MyGlassesTableViewCell *cell = (MyGlassesTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"MyGlassesCell" forIndexPath:indexPath];
        NSManagedObject *glasees = [_myGlassesArray objectAtIndex:indexPath.row];
        
        
        
        
        cell.name.text = [glasees valueForKey:@"name"];
        
        
        NSLog(@"Name is %@" , [glasees valueForKey:@"name"]);
        if (choosenImage) {
            cell.statusImage.image = choosenImage;
        }
        else{
            cell.statusImage.image = [UIImage imageNamed:@"OvalOff"];
        }
        
        cell.goingNextBtn.tag = indexPath.row;
        
        [cell.goingNextBtn addTarget:self action:@selector(goNextPressed:) forControlEvents:UIControlEventTouchUpInside];

        
        for (LGPeripheral *perph in allFoundPerphs) {
            if ([perph.UUIDString isEqualToString:[glasees valueForKey:@"identifier"]]) {
                cell.name.text = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
                cell.statusImage.image = [UIImage imageNamed:@"OvalOn"];
                
                break;
            }
        }
        return cell;
    }
    else{
        AddNewDeviceTableViewCell *cell = (AddNewDeviceTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"AddNewDeviceTableViewCell" forIndexPath:indexPath];
        return cell;
    }
}

- (IBAction)goNextPressed:(UIButton *)sender {
    selectedGlasses = _myGlassesArray[sender.tag];
    [self performSegueWithIdentifier:@"mapId" sender:self];
}





- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    //NSInteger selectedRow = indexPath.row; //this is the number row that was selected
    if (indexPath.row < _myGlassesArray.count) {
        selectedGlasses = _myGlassesArray[indexPath.row];
        //[self performSegueWithIdentifier:@"mapId" sender:tableView];
    }
    else{
        //ToAddNew
        [self performSegueWithIdentifier:@"ToAddNew" sender:tableView];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < _myGlassesArray.count) {
        return 110.0;
    }
    else{
        return 40;
    }
}



-(void)scanWithTime :(int)time {
    dispatch_async(dispatch_get_main_queue(), ^{
        //Your main thread code goes in here
        if (![[LGCentralManager sharedInstance] isCentralReady]) {
            NSLog(@"Reason is %@" ,[[LGCentralManager sharedInstance] centralNotReadyReason] );
            return;
        }
        // Scaning 4 seconds for peripherals
        [[LGCentralManager sharedInstance] scanForPeripheralsByInterval:time
                                                             completion:^(NSArray *peripherals)
         {
             // If we found any peripherals sending to test
             if (peripherals.count) {
                 
                 allFoundPerphs = [[NSMutableArray alloc] initWithArray:peripherals];
                 
                 LGPeripheral *perph = [peripherals firstObject];
                 NSLog(@"BLE FOUND = %ld with name = %@" , (long)perph.RSSI , [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"]);
                 //[self testPeripheral:peripherals[0]];
                 //[self putPeripheralOnFound:perph];
                 [self scanWithTime: 2];
                 
                 [self.glassesTableView reloadData];

             }
             else{
                 NSLog(@"NONE");
                 [self scanWithTime: 2];
                 allFoundPerphs = [[NSMutableArray alloc] init];
                 [self.glassesTableView reloadData];

             }
         }];
        
    });
    
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([segue.identifier isEqualToString:@"mapId"]) {
         MapViewController *vc = segue.destinationViewController;
         vc.selectedGlasses = selectedGlasses;
     }
 }


- (IBAction)editBtnPressed:(id)sender {
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Edit" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Rename" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Remove" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        //[GlassesPeripheral deleteGlasses:self.selectedGlasses];
        
        [self dismissViewControllerAnimated:YES completion:^{
            //[self.navigationController popViewControllerAnimated:YES];
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Set Proximity" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [self performSegueWithIdentifier:@"ToProximity" sender:self];

        
        [self dismissViewControllerAnimated:YES completion:^{
            //[self.navigationController popViewControllerAnimated:YES];
            
        }];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Add Glasses Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController* picker = [[UIImagePickerController alloc] init];
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            [self presentViewController:picker animated:YES completion:NULL];
        }

        
       
        
    }]];
    
    
    //ToProximity
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    choosenImage = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(void)createNavigationTitleViewWithTitle :(NSString *)title {
    UIImage *image = [UIImage imageNamed: @"foundLogoSmall"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: image];
    imageView.frame = CGRectMake(0, -2, 40, 40);
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    
  
    
    CGRect applicationFrame = CGRectMake(0, 0, 40, 40);
    UIView * newView = [[UIView alloc] initWithFrame:applicationFrame] ;
    [newView addSubview:imageView];
    
    self.navigationItem.titleView = newView;
}



@end
