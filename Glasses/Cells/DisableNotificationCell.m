//
//  DisableNotificationCell.m
//  Glasses
//
//  Created by Hamza Temp on 17/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "DisableNotificationCell.h"

@implementation DisableNotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
