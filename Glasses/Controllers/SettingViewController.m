//
//  SettingViewController.m
//  Glasses
//
//  Created by Hamza Temp on 16/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()

@end

@implementation SettingViewController
@synthesize tabBar;

- (void)viewDidLoad {
    self.navigationItem.hidesBackButton = YES;
    [super viewDidLoad];
    self.tabBar.delegate = self;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    // Do Stuff!
    // if(item.title == @"First") {...}
    
    if(item.tag == 0)
    {
        [self popoverPresentationController];
        [self performSegueWithIdentifier:@"glassesList" sender:item];
        
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
