//
//  MyGlassesViewController.m
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "MyGlassesViewController.h"
#import "MyGlassesTableViewCell.h"

@interface MyGlassesViewController ()

@end

@implementation MyGlassesViewController
@synthesize tableView,tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view.
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tabBar.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 3;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MyGlassesTableViewCell *cell = (MyGlassesTableViewCell*)[tableView dequeueReusableCellWithIdentifier: @"MyGlassesCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item {
    // Do Stuff!
    // if(item.title == @"First") {...}
    
   
    if(item.tag == 1)
    {
        [self popoverPresentationController];
        [self performSegueWithIdentifier:@"settings" sender:item];
    }
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger selectedRow = indexPath.row; //this is the number row that was selected
    
        [self performSegueWithIdentifier:@"mapId" sender:tableView];
        
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
