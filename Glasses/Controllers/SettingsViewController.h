//
//  SettingsViewController.h
//  Glasses
//
//  Created by Hamza Temp on 17/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController < UITableViewDelegate,UITabBarDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
