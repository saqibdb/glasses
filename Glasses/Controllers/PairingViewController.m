//
//  PairingViewController.m
//  Glasses
//
//  Created by Hamza Temp on 14/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "PairingViewController.h"
#import "ViewController.h"
#import <LGBluetooth.h>
#import "GlassesPeripheral.h"


@interface PairingViewController (){
    NSMutableArray *myGlassesArray;
    GlassesPeripheral *newGlasses;
}

@end


@implementation PairingViewController

@synthesize midCircle,outerCircle,connectedView,connectingView,closeBtn,doneBtn,timer,tabBar;
- (void)viewDidLoad {
    [super viewDidLoad];
    [LGCentralManager sharedInstance];

    // Do any additional setup after loading the view.
    [self.connectedView setHidden:true];
    [self.connectingView setHidden:false];
    [self.doneBtn setEnabled:false];
    
    [self.midCircle setHidden:true];
    [self.outerCircle setHidden:true];
    
    self.tabBar.delegate = self;
    
    int count = [self.tabBar.items count];
    
    
    for (int i = 0 ; i < count ; i ++)
    {
        UITabBarItem * item =  [self.tabBar.items objectAtIndex:i];
        [item.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    
    
    myGlassesArray = [GlassesPeripheral getAllGlasses];
    
    
    
    [self.view layoutIfNeeded];
    self.pulseBtn.cornerRadius = self.pulseBtn.frame.size.width / 2;
    [self.view layoutIfNeeded];

}

-(void)viewWillAppear:(BOOL)animated {
    [self animateForConnection];

    [self scanWithTime:1];
    
}


-(void)scanWithTime :(int)time {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pulseBtn ceateAGenericPulse];
        //Your main thread code goes in here
        if (![[LGCentralManager sharedInstance] isCentralReady]) {
            NSLog(@"Reason is %@" ,[[LGCentralManager sharedInstance] centralNotReadyReason] );
            return;
        }
        // Scaning 4 seconds for peripherals
        [[LGCentralManager sharedInstance] scanForPeripheralsByInterval:time
                                                             completion:^(NSArray *peripherals)
         {
             // If we found any peripherals sending to test
             if (peripherals.count) {
                 for (LGPeripheral *perph in peripherals) {
                     if (myGlassesArray.count) {
                         NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF.identifier LIKE[cd] %@",perph.UUIDString];

                         NSArray *searchedArray = [myGlassesArray filteredArrayUsingPredicate:resultPredicate];
                         
                         if (!searchedArray.count) {
                             NSLog(@"BLE FOUND = %ld with name = %@" , (long)perph.RSSI , [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"]);

                             [self putPeripheralOnFound:perph];
                             [self showConnectedScreen];
                             return;
                         }

                     }
                     else{
                         [self putPeripheralOnFound:perph];
                         [self showConnectedScreen];
                         return;
                     }
                     
                 }
                 [self scanWithTime: 1];
             }
             else{
                 NSLog(@"NONE");
                 [self scanWithTime: 1];
             }
         }];
        
    });
    
}


-(void)putPeripheralOnFound :(LGPeripheral *) perph{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.glassesNameText.text = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
        self.serialNumberText.text = [NSString stringWithFormat:@"Signal Strength = %li", (long)perph.RSSI];
        
        newGlasses = [[GlassesPeripheral alloc] init];
        newGlasses.name = [perph.advertisingData objectForKey:@"kCBAdvDataLocalName"];
        newGlasses.identifier = perph.UUIDString;
        newGlasses.dateInserted = [NSDate date];
        newGlasses.image = @"";
    });

}




- (void)testPeripheral:(LGPeripheral *)peripheral
{
    // First of all, opening connection
    [peripheral connectWithCompletion:^(NSError *error) {
        // Discovering services of peripheral
        [peripheral discoverServicesWithCompletion:^(NSArray *services, NSError *error) {
            // Searching in all services, our - 5ec0 service
            for (LGService *service in services) {
                if ([service.UUIDString isEqualToString:@"5ec0"]) {
                    // Discovering characteristics of 5ec0 service
                    [service discoverCharacteristicsWithCompletion:^(NSArray *characteristics, NSError *error) {
                        __block int i = 0;
                        // Searching writable characteristic - cef9
                        for (LGCharacteristic *charact in characteristics) {
                            if ([charact.UUIDString isEqualToString:@"cef9"]) {
                                [charact writeByte:0xFF completion:^(NSError *error) {
                                    if (++i == 3) {
                                        [peripheral disconnectWithCompletion:nil];
                                    }
                                }];
                            } else {
                                // Otherwise reading value
                                [charact readValueWithBlock:^(NSData *data, NSError *error) {
                                    if (++i == 3) {
                                        [peripheral disconnectWithCompletion:nil];
                                    }
                                }];
                            }
                        }
                    }];
                }
            }
        }];
    }];
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Animation

-(void)animateForConnection
{
    
    timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(showAnimation) userInfo:nil repeats:true];
}

-(void)showAnimation
{
    if(self.midCircle.isHidden && self.outerCircle.isHidden)
    {
        [self.midCircle setHidden:false];
    }
    else if ( self.midCircle.isHidden == false && self.outerCircle.isHidden)
    {
         [self.midCircle setHidden:false];
         [self.outerCircle setHidden:false];
    }
    else if(self.midCircle.isHidden == false && self.outerCircle.isHidden == false)
    {
        [self.midCircle setHidden:true];
        [self.outerCircle setHidden:true];
    }
}


-(void)showConnectedScreen
{
    [self.connectedView setHidden:false];
    [self.connectingView setHidden:true];
    [self.doneBtn setEnabled:true];
    
    // stop timer
    [timer invalidate];
    timer = nil;
}

#pragma mark - Button Methods
- (IBAction)closeBtnPressed:(id)sender {
    
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    
    CATransition* transition = [CATransition animation];
    transition.duration = .5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionMoveIn; //kCATransitionMoveIn; //, kCATransitionPush, kCATransitionReveal, kCATransitionFade
    transition.subtype = kCATransitionFromBottom; //kCATransitionFromLeft, kCATransitionFromRight, kCATransitionFromTop, kCATransitionFromBottom
    [self.navigationController.view.layer addAnimation:transition forKey:@"someAnimation"];
    
    [self.navigationController popViewControllerAnimated:YES];
    [CATransaction commit];
    
    
    //[self.navigationController popViewControllerAnimated:true];
}
- (IBAction)doneBtnPressed:(id)sender {
    if (newGlasses) {
        NSError *error = [GlassesPeripheral createNewGlassesWithGlasses:newGlasses];
        if (error) {
            NSLog(@"Something is Wrong");
        }
    }
    [self.navigationController popViewControllerAnimated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




@end
