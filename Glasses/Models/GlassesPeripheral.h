//
//  GlassesPeripheral.h
//  Glasses
//
//  Created by iBuildx-Macbook on 05/12/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface GlassesPeripheral : NSObject


@property (nonatomic) NSString *name;
@property (nonatomic) NSString *image;
@property (nonatomic) NSString *identifier;
@property (nonatomic) NSDate *dateInserted;


- (id)initWithManagedObject:(NSManagedObject *)object;
+ (NSError *)createNewGlassesWithGlasses :(GlassesPeripheral *)glasses;
+ (NSMutableArray *)getAllGlasses;
+ (void)deleteGlasses:(GlassesPeripheral *)glasses;

@end
