//
//  GlassesPeripheral.m
//  Glasses
//
//  Created by iBuildx-Macbook on 05/12/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import "GlassesPeripheral.h"
#import <UIKit/UIKit.h>

@implementation GlassesPeripheral



- (id)initWithManagedObject:(NSManagedObject *)object{
    self = [super init];
    if(self){
        self.name = [object valueForKey:@"name"];
        self.image = [object valueForKey:@"image"];
        self.identifier = [object valueForKey:@"identifier"];
        self.dateInserted = [object valueForKey:@"dateInserted"];
    }
    return self;
}


+ (NSError *)createNewGlassesWithGlasses :(GlassesPeripheral *)glasses {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Create a new managed object
    NSManagedObject *newGlasses = [NSEntityDescription insertNewObjectForEntityForName:@"Glasses" inManagedObjectContext:context];
    [newGlasses setValue:glasses.name forKey:@"name"];
    [newGlasses setValue:glasses.image forKey:@"image"];
    [newGlasses setValue:glasses.identifier forKey:@"identifier"];
    [newGlasses setValue:glasses.dateInserted forKey:@"dateInserted"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    return error;
}

+ (NSMutableArray *)getAllGlasses {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    return [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}

+ (void)deleteGlasses:(GlassesPeripheral *)glasses {
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Glasses"];
    fetchRequest.predicate = [NSPredicate predicateWithFormat:@"identifier == %@", glasses.identifier];
    NSArray *array = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    for (NSManagedObject *managedObject in array) {
        [managedObjectContext deleteObject:managedObject];
    }
}


+ (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}


@end
