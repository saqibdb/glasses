//
//  ViewController.h
//  Glasses
//
//  Created by Hamza Temp on 12/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITabBarDelegate, UITableViewDataSource , UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;
@property (weak, nonatomic) IBOutlet UITableView *glassesTableView;

@property(nonatomic, retain) NSMutableArray *myGlassesArray;

@end

