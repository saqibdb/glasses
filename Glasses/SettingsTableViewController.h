//
//  SettingsTableViewController.h
//  Glasses
//
//  Created by iBuildx-Macbook on 23/10/2017.
//  Copyright © 2017 Hamza Temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsTableViewController : UITableViewController<UITableViewDelegate>

@end
